# BinChicken v0.1 alpha
This software is for detecting microorganisms from genome or transcriptome sequencing data and finding intergration sites to other genomes (either human, or any other microorganism detected in the sample).

## How to use
Requirements: python 3, samtools, bwa and/or STAR aligner. All tools must be in your $PATH (i.e. need to be able to run when just typing samtools or bwa in the command promt). Install the required python libraries: `python3 -m pip install -r requirements.txt`

Once this is done, then open helper.htm file in you web browser to create commands to run the tool.

The outputted report file is an HTML file that can be open in web browser. NB! Some temporary files are stored in the temp/ folder and will not be deleted by default at the moment. Please make sure to do it on your own.

Tip: '--data dna' and '--data rna' difference is the used aligner. If you want to use BWA also for RNA-seq data (instead of STAR) then use '--data dna'

### Detect a specific microorganism (virus, bacteria, archea, fungi, etc.)
Choose from list or provide RefSeq or GenBank ID's.

### Detect any virus
To detect any virus you need to provide a reference containing large number of viruses. One good option is to use genomes from viruSITE (http://www.virusite.org/) which has in total of 14813 genomes (as of 8 September 2022). viruSITE comprises all genomes from viruses, viroids and satellites published in NCBI Reference Sequence Database (RefSeq). To obtain the reference, run the following code which will download the genomes, unpacks and replaces the genome headers to fit with BinChicken. The final file name has to be 'reference.fa' in the reference file folder.

```
mkdir virusite
cd virusite
wget http://www.virusite.org/archive/2022.2/genomes.fasta.zip
unzip genomes.fasta.zip
sed -i -e 's/refseq|\(.*\)|.*nt|\(.*\)/\1 \2/' genomes.fasta
mv genomes.fasta reference.fa
```

Another set of viruses and bacteria was downloaded from NCBI by using https://gitlab.com/andreassh/microbial-genomics/-/blob/main/downloadNCBIgenomes.sh script.

## TODO/Ideas
- Improvements and fixing issues in report
- Revise provided groups (pathogenic bacteria, fungi and protozoa)
- Provide a list of targeted species (useful when using groups, which otherwise has to be checked freom reference.microindex file)
- Docker container
- Detect integrated sequences without reference genome?

# Contact
Contact: andreas@stripy.org