FROM ubuntu:20.04

LABEL \
  version="0.1" \
  description="binchicken"

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/London
  
# Run update and install necessary libraries, including BWA and python3
RUN apt-get update -y && apt-get install -y --no-install-recommends \
    build-essential \
    bwa \
    bzip2 \
    unzip \
    ca-certificates \
    gcc \
    git \
    libbz2-dev \
    libcurl4-openssl-dev \
    libffi-dev \
    liblzma-dev \
    libncurses5-dev \
    libssl-dev \
    make \
    python3 \
    python3-dev \
    python3-pip \
    software-properties-common \
    wget \
    zlib1g-dev

WORKDIR /usr/local/bin/

# Install Samtools
ENV SAMTOOLS_VERSION="1.15.1"
RUN wget https://github.com/samtools/samtools/releases/download/${SAMTOOLS_VERSION}/samtools-${SAMTOOLS_VERSION}.tar.bz2 \
    && tar xjf samtools-${SAMTOOLS_VERSION}.tar.bz2 \
    && rm samtools-${SAMTOOLS_VERSION}.tar.bz2 \
    && cd samtools-${SAMTOOLS_VERSION} \
    && ./configure \
    && make \
    && make install \
    && make clean

WORKDIR /usr/local/bin/

# Install STAR
RUN wget https://github.com/alexdobin/STAR/releases/download/2.7.10a_alpha_220818/STAR_2.7.10a_alpha_220818_Linux_x86_64_static.zip \
    && unzip STAR_2.7.10a_alpha_220818_Linux_x86_64_static.zip

# Install BinChicken by cloning from git repo
RUN git clone https://gitlab.com/andreassh/binchicken.git \
    && python3 -m pip install -r binchicken/requirements.txt

WORKDIR /usr/local/bin/binchicken