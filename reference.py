import re
import os
import csv
import json
import math
import argparse
import subprocess
import urllib.request as request
from urllib.request import urlopen
from zipfile import ZipFile
from contextlib import closing

def safe_open_w(path, mode = "w"):
	# Open path for writing while creating any parent directories as needed
	os.makedirs(os.path.dirname(path), exist_ok = True)
	return open(path, mode, encoding = 'utf-8')

def safe_open_binary(path, mode = "wb"):
	# Open path for writing while creating any parent directories as needed
	os.makedirs(os.path.dirname(path), exist_ok = True)
	return open(path, mode)

def download_dataset(ids, fasta_path_out, datatype):
	# Download FASTAs and GFF (if available) from NCBI using assembly IDs
	print("Starting to download files from NCBI")

	# Folders	
	tempdir = 'temp/'
	unpacked_dir = os.path.join(tempdir, 'downloads/')
	zipfile_path = os.path.join(unpacked_dir, "data.zip")

	# Download files by using NCBI API
	url = 'https://api.ncbi.nlm.nih.gov/datasets/v1/genome/accession/'+ids+'/download?include_annotation_type=GENOME_GFF'

	zipresp = urlopen(url)
	tempzip = safe_open_binary(zipfile_path, 'wb')
	tempzip.write(zipresp.read())
	tempzip.close()
	
	# Unzip files
	zf = ZipFile(zipfile_path)
	zf.extractall(path = unpacked_dir)
	zf.close()

	# Open the file that contains file paths of downloaded files
	download_info = os.path.join(unpacked_dir, "ncbi_dataset/data/dataset_catalog.json")

	with open(download_info, 'r') as f:
		data = json.load(f)
		assemblies = data["assemblies"]

	fastas_path, gffs_path = [], []

	for a in assemblies:
		for f in a["files"]:
			if f["fileType"] == "GENOMIC_NUCLEOTIDE_FASTA":
				fastas_path.append(os.path.join(unpacked_dir, 'ncbi_dataset/data/' + f["filePath"]))
			elif f["fileType"] == "GFF3":
				gffs_path.append(os.path.join(unpacked_dir, 'ncbi_dataset/data/' + f["filePath"]))

	# Get each fasta content and create a new combined fasta reference file
	info_arr = {}
	total_len_genome = 0
	combined_fasta = ''

	for fasta in fastas_path:
		with open(fasta, 'r') as f:
			fasta = f.read()

			# Get the contig ID and following sequence from the FASTA file
			header_line = fasta.partition('\n')[0]
			sequence = ''.join(fasta.partition('\n')[1:]).replace("\n", "")

			header_re = re.search(">(.*?) (.*?)$", header_line)
			contig_id = header_re.group(1)
			contig_name = header_re.group(2)
			contig_length = len(sequence)

			total_len_genome += contig_length

			combined_fasta += fasta

			# Put together info about this contig and later add this into a file
			info_arr.update({contig_id: {
				"Name": contig_name,
				"GenomeLength": contig_length
			}})

	# Save FASTA
	fasta_file_out = os.path.join(fasta_path_out, 'reference.fa')
	with safe_open_w(fasta_file_out) as f:
		f.write(combined_fasta)

	# Create combined regions JSON file from GFFs (only take genes)
	all_regions = {}

	for gff in gffs_path:
		regions, accID = {}, ''
		with open(gff, 'r') as rf:
			gff_content = csv.reader(rf, delimiter = '\t')
			for line in gff_content:
				if len(line) > 2:
					if line[2] == "gene":
						accID = line[0]
						gene_name = line[8].split("Name=")[1].split(";")[0] if "Name" in line[8] else line[8].split(";description=")[1].split(";")[0]
						regions.update({gene_name: {
							"Start": int(line[3]),
							"End": int(line[4])
						}})

		all_regions.update({accID: regions})

	# Save regions file on disk
	gff_file_out = os.path.join(fasta_path_out, 'reference.microregions')
	if os.path.exists(gff_file_out):
		with safe_open_w(gff_file_out) as f:
			f.write(json.dumps(all_regions, indent = 4))

	indexReference(datatype, fasta_path_out, fasta_file_out, total_len_genome)

	return info_arr

def download_accFastas(ids, fasta_path_out, datatype):
	# Download FASTAs from NCBI using accession IDs
	print("Starting to download files from NCBI")

	url = 'http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nucleotide&rettype=fasta&retmode=text&id=' + ids

	with closing(request.urlopen(url)) as r:
		fasta_content = r.read().decode("utf-8") 

	info_arr = {}
	fastas_splitted = fasta_content.split("\n\n")

	total_len_genome = 0
	for fasta in fastas_splitted:
		if len(fasta) < 10: # Make sure it's not empty so at least some content is required
			continue

		# Get the contig ID and following sequence from the FASTA file
		header_line = fasta.partition('\n')[0]
		sequence = ''.join(fasta.partition('\n')[1:]).replace("\n", "")

		header_re = re.search(">(.*?) (.*?)$", header_line)
		contig_id = header_re.group(1)
		contig_name = header_re.group(2)
		contig_length = len(sequence)

		total_len_genome += contig_length

		# Put together info about this contig and later add this into a file
		info_arr.update({contig_id: {
			"Name": contig_name,
			"GenomeLength": contig_length
		}})

	# Save the aquired FASTA file
	fasta_file_out = os.path.join(fasta_path_out, 'reference.fa')

	with safe_open_w(fasta_file_out) as fastafile:
		fastafile.write(fasta_content.replace("\n\n", "\n"))

	indexReference(datatype, fasta_path_out, fasta_file_out, total_len_genome)

	return info_arr


def indexReference(datatype, fasta_path_out, fasta_file_out, total_len_genome):
	print("Starting to index the reference genome")

	# Index the FASTA with STAR
	if datatype == "rna" or datatype == "scrna" or datatype == "xna":
		print("Indexing with STAR")

		fasta_path_out_star = os.path.join(fasta_path_out, 'star/')

		index_n_bases = min(14, math.log2(total_len_genome)/2 - 1) # Calculate parameter for genomeSAindexNbases based on the genome size

		subprocess.call(['STAR', '--runMode', 'genomeGenerate', 
								 '--genomeDir', fasta_path_out_star, 
								 '--genomeFastaFiles', fasta_file_out,
								 '--genomeSAindexNbases', str(index_n_bases)
						], stdout = subprocess.DEVNULL)

	# Index the FASTA file with BWA
	if datatype == "dna" or datatype == "xna":
		print("Indexing with BWA")

		subprocess.call(['bwa', 'index', fasta_file_out], stdout = subprocess.DEVNULL)
	
	return True


def index_fasta(fasta_path, datatype):
	print("Creating index for BinChicken")

	fasta_file_in = os.path.join(fasta_path, 'reference.fa')
	with open(fasta_file_in, 'r') as f:
		lines = f.readlines()

	info_arr = {}
	contig_id = ''
	contig_name = ''
	contig_length = 0
	
	for line in lines:
		if line.startswith(">"):
			header_re = re.search(">(.*?) (.*?)$", line)
			contig_id = header_re.group(1)
			contig_name = header_re.group(2)
			contig_length = 0

			info_arr.update({contig_id: {
				"Name": contig_name,
				"GenomeLength": 0
			}})
		else:
			contig_length += len(line.replace("\n", ""))
			info_arr[contig_id]["GenomeLength"] = contig_length

	# Index the FASTA file with BWA
	if datatype == "dna" or datatype == "xna":
		print("Indexing with BWA")

		subprocess.call(['bwa', 'index', fasta_file_in], stdout = subprocess.DEVNULL)

	# Index the FASTA with STAR
	if datatype == "rna" or datatype == "scrna" or datatype == "xna":
		print(datatype)
		print("Indexing with STAR")

		fasta_path_out_star = os.path.join(fasta_path, 'star/')

		total_len_genome = sum(x['GenomeLength'] for x in info_arr.values())
		index_n_bases = min(14, math.log2(total_len_genome)/2 - 1) # Calculate parameter for genomeSAindexNbases based on the genome size

		subprocess.call(['STAR', '--runMode', 'genomeGenerate', 
								'--genomeDir', fasta_path_out_star, 
								'--genomeFastaFiles', fasta_file_in,
								'--genomeSAindexNbases', str(index_n_bases)
						], stdout = subprocess.DEVNULL)

	return info_arr


def saveIndex(content, fasta_folder_path):
	output_index_file = os.path.join(fasta_folder_path, 'reference.microindex')

	with safe_open_w(output_index_file) as f:
		 f.write(json.dumps(content, indent = 4))

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("--do",		    required = True, 	choices = ["download", "index"],    		help = "Download new FASTAs or index the current one?")
	parser.add_argument("--data",		required = False, 	choices = ["dna", "rna", "scrna", "xna"],   help = "Sequencing data type")
	parser.add_argument("--ids",		required = False, 												help = "Accession IDs of genomes when downloading FASTAs")
	parser.add_argument("--fasta",		required = False,												help = "Input FASTA file path when indexing one")
	parser.add_argument("--output",		required = False,												help = "Output FASTA file path where the downloaded file will be saved")
	args = parser.parse_args()
	
	if args.do == "download" and args.ids and args.output:
		ncbi_db = "genbank" if args.ids[0:3] in ["GCA", "GCF"] else "refseq"
		if ncbi_db == "genbank":
			downloaded_info = download_dataset(ids = args.ids, fasta_path_out = args.output, datatype = args.data)
		else:
			downloaded_info = download_accFastas(ids = args.ids, fasta_path_out = args.output, datatype = args.data)

		if downloaded_info:
			saveIndex(content = downloaded_info, fasta_folder_path = args.output)
		else:
			print("Error")

	if args.do == "index" and args.fasta:
		print("Indexing the selected genome")
		indexed_info = index_fasta(args.fasta, args.data)

		if indexed_info:
			saveIndex(content = indexed_info, fasta_folder_path = args.fasta)
		else:
			print("Error")
	
	print("Finished")
