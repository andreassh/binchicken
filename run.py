import re
import os
import json
import pysam
import platform
import argparse
import subprocess
import numpy as np
from itertools import groupby
from datetime import datetime
from collections import Counter
from operator import itemgetter

def safe_open_w(path):
	# Open path for writing while creating any parent directories as needed
	os.makedirs(os.path.dirname(path), exist_ok = True)
	return open(path, 'w', encoding = 'utf-8')

def processSample(filetype, datatype, sample_path, reference_fasta_folder):
	# Take user's sample file name without extension
	sample_path = sample_path.split(',')
	sample_filename = sample_path[0].split('/')[-1]
	sample_filename = sample_filename.split('.')[0]

	scriptpath = os.path.dirname(os.path.abspath(__file__))
	temp_folder = os.path.join(scriptpath, 'temp/')
	# If bam then extract unaligned reads into a new bam file
	if filetype == "bam":
		# Extract unaligned reads
		sample_path = sample_path[0] # Only first element is bam file because only one can be specified

		# Get only paired unaligned reads (both reads unmapped)
		unaligned_bam_file_path = os.path.join(temp_folder, sample_filename + '.pairs-unaligned.bam')
		sample_unaligned_reads_bam = open(unaligned_bam_file_path, 'wb')
		subprocess.call(['samtools', 'view', '-b', '-f', '12', sample_path], stdout = sample_unaligned_reads_bam)

		# Get only paired aligned reads where mate is unmapped but one read is mapped
		aligned_bam_file_path1 = os.path.join(temp_folder, sample_filename + '.pairs-aligned1.bam')
		sample_aligned_reads_bam1 = open(aligned_bam_file_path1, 'wb')
		subprocess.call(['samtools', 'view', '-b', '-f', '4', '-F', '8', sample_path], stdout = sample_aligned_reads_bam1) # Take all unmapped reads and filter out reads where mate is unmapped

		aligned_bam_file_path2 = os.path.join(temp_folder, sample_filename + '.pairs-aligned2.bam')
		sample_aligned_reads_bam2 = open(aligned_bam_file_path2, 'wb')
		subprocess.call(['samtools', 'view', '-b', '-f', '8', '-F', '4', sample_path], stdout = sample_aligned_reads_bam2) # Take all read pairs where mate is unmapped and filter out reads where mate is unmapped
		subprocess.call(['samtools', 'index', aligned_bam_file_path2]) # Index this file as we need to use it later when finding integration sites

		# Merge files
		merged_bam_file_path = os.path.join(temp_folder, sample_filename + '.pairs-merged.bam')
		subprocess.call(['samtools', 'merge', '-f', merged_bam_file_path, unaligned_bam_file_path, aligned_bam_file_path1, aligned_bam_file_path2], stdout = subprocess.DEVNULL)

		# Sort merged file by read names
		merged_sorted_bam_file_path = os.path.join(temp_folder, sample_filename + '.pairs-merged.sorted.bam')
		sample_merged_sorted_bam = open(merged_sorted_bam_file_path, 'wb')
		subprocess.call(['samtools', 'sort', '-n', merged_bam_file_path], stdout = sample_merged_sorted_bam)

		# Convert reads from bam file to fastq
		unaligned_fastq_file_path = [os.path.join(temp_folder, sample_filename + '.unaligned.fq')] # Paired end plus singletons
		subprocess.call(['samtools', 'bam2fq', '-o', unaligned_fastq_file_path[0], merged_sorted_bam_file_path], stdout = subprocess.DEVNULL, stderr = subprocess.DEVNULL)


	elif filetype == "fastq":
		unaligned_fastq_file_path = sample_path
		aligned_bam_file_path2 = None

	# Realign on the microbes reference genome
	realigned_bam_file_path = os.path.join(temp_folder, sample_filename + '.realigned.bam')
	sample_realigned_bam = open(realigned_bam_file_path, 'wb')

	if datatype == "rna" or datatype == "scrna":
		# If the input is gz file then need to use specific command for STAR so it can read gzipped files
		if unaligned_fastq_file_path[0].endswith('.gz'):
			os_name = platform.system()
			zcat_type = 'gzcat' if os_name == "Darwin" else 'zcat' # If mac (Darwin) then use gzcat, otherwise zcat (linux)
			read_files_command = ['--readFilesCommand', zcat_type]
		else:
			read_files_command = []

		# Align with STAR
		reference_fasta_folder_star = os.path.join(reference_fasta_folder + 'star/')
		align = subprocess.Popen(['STAR', 
									'--genomeDir', reference_fasta_folder_star,
									'--outSAMmapqUnique', '60',
#									'--outFileNamePrefix', 'temp/', # Sometimes causing an error (this line) ??
									'--outStd', 'SAM'] +
									read_files_command +
									['--readFilesIn'] +  
									unaligned_fastq_file_path, 
								stdout = subprocess.PIPE, stderr = subprocess.DEVNULL)

	else:
		reference_fasta_file_bwa = os.path.join(reference_fasta_folder, 'reference.fa')
		align = subprocess.Popen(['bwa', 'mem', '-t', '8', '-p', reference_fasta_file_bwa]+unaligned_fastq_file_path, stdout = subprocess.PIPE, stderr = subprocess.DEVNULL)
	
	samsort = subprocess.Popen(['samtools', 'sort', '-'], stdin = align.stdout, stdout = subprocess.PIPE)
	samview = subprocess.Popen(['samtools', 'view', '-Sb'], stdin = samsort.stdout, stdout = sample_realigned_bam)
	samview.wait()

	subprocess.call(['samtools', 'index', realigned_bam_file_path], stdout = subprocess.DEVNULL)
	
	return realigned_bam_file_path, aligned_bam_file_path2 # The latter one is the intermediate file where we can look for reads aligned on host genome


# Get read counts for each contig present in the bamfile
def contig_read_counts(filepath, minreads = 0):
	bamfile = pysam.AlignmentFile(filepath, "rb")
	idxstats = bamfile.get_index_statistics()

	contigs_with_reads = []
	contigs_to_exclude = re.compile("^chr\d{1,2}$|^chrX$|^chrY$|^chrM$|^chr[\d|\w]*_|^\d{1,2}$|^X$|^Y$|^M$") # Human contigs including those with "chr" and those without

	for i in idxstats:
		if i.mapped > minreads and not contigs_to_exclude.match(i.contig): # Include those where there are some reads and exclude human contigs
			contigs_with_reads.append([i.contig, i.mapped])
#            conlen = {x: bamfile.get_reference_length(x) for x in contigs_with_reads}
	bamfile.close()

	return contigs_with_reads


# Get stats about a contig, such as mapped reads count, contig length, coverage percentage and average mapping quality
def contig_stats(filepath, filepath_intermediate, filepath_original, contigs, duplicates = True, detect_integrations = False):
	bamfile = pysam.AlignmentFile(filepath, "rb")
	contig_lengths = dict(zip(bamfile.references, bamfile.lengths))
	out = {}

	for contig in contigs:
		contig_name = contig[0]
		contig_mapped_reads = contig[1]
		contig_len = contig_lengths[contig_name]

		no_inferReadLength = 0
		start = 0
		end = contig_len

		if duplicates is True:
			mapq = []
			readsCount = 0
			total_read_length = 0
			for read in bamfile.fetch(contig_name, start, end):
				if read.is_secondary is False:
					if read.is_supplementary is False:
						try:
							total_read_length += read.infer_query_length()
						except TypeError:
							no_inferReadLength += 1
							continue
						if read.mapping_quality > 0: # Exclude reads which has mapping quality of zero
							mapq.append(read.mapping_quality)
							readsCount += 1

		else:
			mapq = []
			readsCount = 0
			total_read_length = 0
			for read in bamfile.fetch(contig_name, start, end):
				if read.is_secondary is False:
					if read.is_supplementary is False:
						if read.is_duplicate is False:
							try:
								total_read_length += read.infer_query_length()
							except TypeError:
								no_inferReadLength += 1
								continue
							if read.mapping_quality > 0: # Exclude reads which has mapping quality of zero
								mapq.append(read.mapping_quality)
								readsCount += 1

		coveragePercent, posCovered, posTable = get_coverage(bamfile, contig_len, contig_name, getTable=True, minCovForTable = minCoverageBaseCalculation)

		if detect_integrations:
			integrationData = findIntegrationSites(filepath, contig_name)
			host_stats_data = host_stats(filepath, filepath_intermediate, filepath_original, contig_name)

		else:
			# Return empty string
			integrationData = { "Source": [], "SourceText": [], "Target": [], "TargetText": [], "TargetGenome": [], "TargetGenomeText": [], "TargetSort": [] }
			host_stats_data = {}

		if posCovered > 0 and coveragePercent > minCoverageInclusionResults: # Some cases even when there are reads then bases are not covered and this creates an error
			out.update({ 
				contig_name: {
				"Contig": contig_name,
				"ContigLength": contig_len,
				"ReadsCount": readsCount,
				"BasesCovered": posCovered,
				"Coverage": round(coveragePercent, 2), # Percentage
				"MeanMapQ": round(np.mean(np.asarray(mapq)), 1) if len(mapq) > 0 else 0, # Average
				"CovPerBase": posTable, # Average
				"Integrations": integrationData,
				"HostIntegrations": host_stats_data
			}})
	
	bamfile.close()

	return out

# Get coverage, number of covered bases and pileup table
def get_coverage(bamfile, contigLen, contig, start = None, end = None, getTable = False, minCovForTable = 0):
	contigLen = contigLen if contigLen else dict(zip(bamfile.references, bamfile.lengths))[contig]
	posCovered = 0
	posTable = {}

	if (start and end):
		pileup = bamfile.pileup(str(contig), int(start), int(end), stepper = 'all')
	else:
		pileup = bamfile.pileup(region = str(contig), stepper = 'all')

	# If it is a big genome, then take every n-th element
	step_val = 1
	if (contigLen > 10000): 
		step_val = 50
	if (contigLen > 100000): 
		step_val = 100
	if (contigLen > 1000000): 
		step_val = 1000
	
	# Empty pos table with zeros
	if step_val == 1:
		for i in range(0, contigLen, step_val):
			posTable[i] = 0

	# Go through each base that is covered
	meancovarr = []
	
	for x in pileup:
		mapqual = x.get_mapping_qualities()
		mapqual_mean = round(sum(mapqual) / len(mapqual), 0) if sum(mapqual) > 0 else 0 # Take sum of the mapping qualities covering this base
		posCovered += 1

		if (contigLen > 10000): # If it is a big genome, then take the mean of a window
			meancovarr.append(x.n) # Add base coverage into array
			if x.pos % step_val == 0: # If it is divisible by n then continue (i.e. after every nth element)
				posTable[x.pos] = [ round(sum(meancovarr) / len(meancovarr), 0), 
									mapqual_mean
								] # Put the means for the current position
				meancovarr = [] # Empty the array
		else:
			posTable[x.pos] = [x.n, mapqual_mean] # Otherwise take every base

	percentage = posCovered/contigLen*100

	if percentage >= minCovForTable and getTable:
		return percentage, posCovered, posTable
	else:
		return percentage, posCovered, None


def addGenomeNameToDict(reference_folder, results_dict):
	try:
		reference_file_index = os.path.join(reference_folder, 'reference.microindex')

		with open(reference_file_index + '', 'r') as f:
			genomeindex = json.load(f)
		
		for key in results_dict.keys():
			if key in genomeindex:
				results_dict[key]["Name"] = genomeindex[key]["Name"]
			else:
				results_dict[key] = genomeindex[key]

		return results_dict

	except:
		False

def findIntegrationSites(file, contig):
	bamfile = pysam.AlignmentFile(file, 'rb')

	intPositions = { "Source": [], "SourceText": [], "Target": [], "TargetText": [], "TargetGenome": [], "TargetGenomeText": [], "TargetSort": [] }

	# Integration into a genome that is also in reference
	for read in bamfile.fetch(contig = contig):
		if read.cigarstring:
			if "S" in read.cigarstring or "H" in read.cigarstring:
				positions = addToPositions(read, contig, intPositions)
				if positions:
					intPositions.update(positions)

	intPositions.update(filterResults(intPositions, minreads = 1, contig = contig)) # Filter small matches out
	sortDict(intPositions, 'TargetSort')

	return intPositions

def filterResults(data, minreads, contig):
	# Filter out those groups where there are less than 2 reads
	groups = Counter(data["TargetText"])
	toEliminate = []
	for group, count in groups.items():
		if count < minreads: # or contig in group:
			toEliminate.append(group)

	target_texts = data["TargetText"]

	key_indexes_to_pop = []
	for k, i in enumerate(list(target_texts)):
		if i in toEliminate:
			key_indexes_to_pop.append(k)

	filtered = {"Source": [], "SourceText": [], "Target": [], "TargetText": [], "TargetGenome": [], "TargetGenomeText": [], "TargetSort": []}
	for key in data:
		for index, element in enumerate(data[key]):
			if index not in key_indexes_to_pop:
				filtered[key].append(element)

	return filtered

def inferReadLength(cigar_string):
	read_consuming_ops = ("M", "I", "=", "X")
	result = 0
	cig_iter = groupby(cigar_string, lambda chr: chr.isdigit())
	for _, length_digits in cig_iter:
		length = int(''.join(length_digits))
		op = next(next(cig_iter)[1])
		if op in read_consuming_ops:
			result += length
			
	return result

def addToPositions(read, contig, intPositions):
	readpos_source = ''
	sa_reference_name = ''
	mate_readpos = ''

	if read.is_paired and read.has_tag('SA'):
		sa_tag = read.get_tag("SA").split(",")
		sa_reference_name = sa_tag[0]
		read_len_inferred = inferReadLength(read.cigarstring)

		# When mate mapped to another contig
		if read.mate_is_mapped: #and sa_reference_name != contig: # temporary
			if read.is_forward and read.next_reference_name == contig:
				if read.next_reference_start < read.reference_start:
					readpos_source = read.reference_start + read_len_inferred
				else:
					readpos_source = read.reference_start 

			if read.is_reverse and read.next_reference_name == contig:
				if read.next_reference_start > read.reference_start:
					readpos_source = read.reference_end - read_len_inferred
				else:
					readpos_source = read.reference_end

			if read.is_forward and read.next_reference_name != contig:
				readpos_source = read.reference_end

			if read.is_reverse and read.next_reference_name != contig:
				readpos_source = read.reference_end - read_len_inferred

			read_len_from_sa = inferReadLength(sa_tag[3])

			if read.mate_is_mapped and read.is_forward and read.next_reference_name == contig:
				if read.reference_start < int(sa_tag[1]):
					if sa_tag[2] == "+":
						mate_readpos = int(sa_tag[1]) + read_len_from_sa
					else:
						mate_readpos = int(sa_tag[1]) 
				else:
					if sa_tag[2] == "+":
						mate_readpos = int(sa_tag[1]) if sa_reference_name == contig else int(sa_tag[1]) + read_len_from_sa
					else:
						mate_readpos = int(sa_tag[1]) + read_len_from_sa
	
			elif read.mate_is_mapped and read.is_reverse and read.next_reference_name == contig:
				if read.reference_start <= int(sa_tag[1]):
					mate_readpos = int(sa_tag[1]) + read_len_from_sa
				else:
					mate_readpos = int(sa_tag[1])

			elif read.mate_is_mapped and read.is_forward and read.next_reference_name != contig:
				if sa_tag[2] == "+":
					mate_readpos = int(sa_tag[1])
				else:
					mate_readpos = int(sa_tag[1]) + read_len_from_sa

			elif read.mate_is_mapped and read.is_reverse and read.next_reference_name != contig:
				if sa_tag[2] == "+":
					mate_readpos = int(sa_tag[1]) 
				else:
					mate_readpos = int(sa_tag[1]) + read_len_from_sa 

			read_location = read.reference_name + ' ' + str(readpos_source)
			mate_location = sa_reference_name + ' ' + str(mate_readpos) if read.mate_is_mapped else 'Unaligned'

			intPositions["Source"].append(int(readpos_source))
			intPositions["SourceText"].append(read_location)
			intPositions["Target"].append(int(mate_readpos))
			intPositions["TargetText"].append(mate_location)
			intPositions["TargetGenomeText"].append(sa_reference_name)

	return intPositions

def sortDict(dict_, refkey):
# Sort dict arrays based on the TargetSort array
	reflist = sorted([(v, i) for i, v in enumerate(dict_[refkey])], key=itemgetter(0))
	for v in dict_.values():
		v_ = v[:]
		for i, (_, p) in enumerate(reflist):
			v[i] = v_[p]


# Section for analysing reads that have unmapped read (i.e. likely integrated into the host genome)
def getCoverage(file, coordinates):
# Get coverage, number of covered bases and pileup table
	bamfile = pysam.AlignmentFile(file, 'rb')
	combined_table = {}

	for contig in coordinates:
		flanking_end = 1000 # bp
		start = coordinates[contig]["Start"] - flanking_end
		end = coordinates[contig]["End"] + flanking_end

		region_len = end-start # In base pairs
		step_val = 1
		if (region_len > 10000): 
			step_val = 50
		if (region_len > 100000): 
			step_val = 100
		if (region_len > 1000000): 
			step_val = 1000
		
		cov = bamfile.count_coverage(contig, start, end, quality_threshold = 0)
		combined_cov = [a+b+c+d for a, b, c, d in zip(cov[0], cov[1], cov[2], cov[3])] # Add 4 nucleotides together
		positions = list(range(start, end, 1))

		combined_table[contig] = {}
		combined_list = {}


		for i, v in enumerate(positions):
			combined_list[v] = combined_cov[i]

		if step_val > 1:
			meancovarr = []
			combined_list_means = {}
			curr_no = 0

			for i in combined_list:
				curr_no += 1
				meancovarr.append(combined_list[i])
				if curr_no % step_val == 0: # If it is divisible by n then continue (i.e. after every nth element)
					combined_list_means[i] = round(sum(meancovarr) / len(meancovarr), 0)
					meancovarr = []
			
			combined_list = combined_list_means # Replace the list with shorter list with means

		combined_table[contig] = combined_list

	return combined_table


def getUnmappedReadNames(file, contig):
	# Gets the read names of unmapped reads
	unmapped_read_names = []
	bamfile = pysam.AlignmentFile(file, 'rb')

	for read in bamfile.fetch(contig = contig):
		if read.mate_is_unmapped:
			unmapped_read_names.append(read.query_name)

	unmapped_read_names = set(unmapped_read_names)
	bamfile.close()
	
	return unmapped_read_names

def getUnmappedReadPairOriginLocation(file, readlist):
	# Finds mates for reads that are unaligned in viral-aligned data and gets the location range of those reads
	bamfile = pysam.AlignmentFile(file, 'rb')
	sites = []

	for read in bamfile.fetch():
		if read.query_name in readlist:
			sites.append((read.reference_name, read.reference_start, read.reference_end))
	
	result = {}
	for chr, start, end in sites:
		if chr not in result:
			result[chr] = {"Start": start, "End": end}
		else:
			if start < result[chr]["Start"]:
				result[chr]["Start"] = start
			if end > result[chr]["End"]:
				result[chr]["End"] = end

	bamfile.close()

	return result

def findUnmappedReadPairIntegrationSites(file, regions, source):
	# Find unmapped read pair integration sites in the host genome - just looking all reads in the given region and using the one which are clipped
	bamfile = pysam.AlignmentFile(file, 'rb')
	integrationData = { "Source": [], "SourceText": [], "Target": [], "TargetText": [], "TargetGenomeText": [] }

	for chr in regions:
		for read in bamfile.fetch(chr, regions[chr]["Start"], regions[chr]["End"]):
			if read.cigarstring:
				if any(c in ["S", "H"] for c in read.cigarstring):
					if read.is_forward:
						readpos = read.reference_end
					elif read.is_reverse:
						readpos = read.reference_start

					integrationData["Source"].append(source)
					integrationData["Target"].append(readpos)
					integrationData["TargetGenomeText"].append(chr)
					integrationData["TargetText"].append(chr + ' ' + str(readpos))
	bamfile.close()

	return integrationData


def host_stats(realigned_sample_file, intermediate_sample_file, original_sample_file, contig):
	host_data = {
		"Integrations": {},
		"Coverage": {}
	}

	if intermediate_sample_file:
		unmapped_reads = getUnmappedReadNames(realigned_sample_file, contig) # Get unmapped read names
		unmapped_read_origin_locations = getUnmappedReadPairOriginLocation(intermediate_sample_file, unmapped_reads) # Find region in the intermediate file by using unmapped read names
		unmapped_read_origin_int_sites = findUnmappedReadPairIntegrationSites(intermediate_sample_file, unmapped_read_origin_locations, source = contig) # Search for integration sites in the original file by using the region location
		unmapped_read_origin_coverage = getCoverage(sample_file, unmapped_read_origin_locations) # Search for integration sites in the original file by using the region location

		host_data["Integrations"] = unmapped_read_origin_int_sites
		host_data["Coverage"] = unmapped_read_origin_coverage

	return host_data


if __name__ == '__main__':

	parser = argparse.ArgumentParser()
	parser.add_argument("--data",           required = True,    choices = ["dna", "rna", "scrna"],  help = "Sequencing data type")
	parser.add_argument("--reference",      required = True,                                        help = "Input reference FASTA file path that contains microbe genomes")
	parser.add_argument("--sample",         required = True,                                        help = "Input sample file to analyse")
	parser.add_argument("--filetype",       required = True,    choices = ["bam", "fastq"],         help = "Input sample file type")
	parser.add_argument("--integration",    required = False,                                       help = "Detect integrations?")
	parser.add_argument("--output",         required = True,                                        help = "Output folder where the report file will be saved")
	args = parser.parse_args()

	# Config
	if args.data == "dna":
		minNoReads = 5
		minMapQ = 20
		minDepth = 1
		minCoverageInclusionResults = 1 # Percent
		minCoverageBaseCalculation = 1 # Percent
	elif args.data == "rna":
		minNoReads = 5
		minMapQ = 20
		minDepth = 1
		minCoverageInclusionResults = 0.01 # Percent
		minCoverageBaseCalculation = 0.01 # Percent

	sample_file = args.sample
	print("Aligning the sample on microbes' genome")
	realigned_sample_file, intermediate_sample_file = processSample(filetype = args.filetype, datatype = args.data, sample_path = sample_file, reference_fasta_folder = args.reference)
	print("Analysing results and creating the report")

	if realigned_sample_file:
		sample_contigs_with_reads = contig_read_counts(realigned_sample_file, minreads = minNoReads) # Get all contigs and number of mapped reads
		contigs_stats_data = contig_stats(realigned_sample_file, intermediate_sample_file, sample_file, sample_contigs_with_reads, detect_integrations = args.integration) # Get info about the conting and find inegrations

		# Add genome's name into the results dict for the particular contig
		addGenomeNameToDict(reference_folder = args.reference, results_dict = contigs_stats_data)

		time_now = datetime.now().strftime("%d-%m-%Y")
		sample_name = os.path.splitext(sample_file)[0].split("/")[-1]
		report_file_name = sample_name+'_' + time_now + '.htm'

		output_json = json.dumps(contigs_stats_data, indent = 4)
		scriptpath = os.path.dirname(os.path.abspath(__file__))

		with open(os.path.join(scriptpath, 'assets/results_template.htm'), 'rt') as resultsHTMLtemplate:
			gene_features = '{}'
	
			# Gene regions - open regions file and write into another gene features JS file
			microregions_file = os.path.join(args.reference, 'reference.microregions')
			if os.path.exists(microregions_file):
				with open(microregions_file, 'r') as f:
					gene_features = json.dumps(json.load(f), indent = 4)

			with open(os.path.join(args.output, report_file_name), 'wt') as outputHTMLfile:
				for line in resultsHTMLtemplate:
					output_text = line.replace('/*SAMPLERESULTS*/', output_json).replace('/*GENEFEATURES*/', gene_features)
					outputHTMLfile.write(output_text)
		
		print("Finished")
		output_full_path = os.path.realpath(args.output)
		sample_file_name = 'sample'
