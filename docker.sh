#!/bin/bash

PROGNAME=$0

usage() {
  cat << EOF >&2
Usage: $PROGNAME -parameters

  -d <data>:                (Required) Data type (DNA or RNA)
  -t <filetype>:            (Required) Filetype (BAM or FASTQ)
  -r <reference>:           (Required) Reference genome (FASTA file)
  -o <output>:              (Required) Output folder where report will be saved
  -f <findIntegrations>:    (Optional) Find integrations?
  -i <input>:               (Required) Input file(s) (indexed BAM or CRAM)
  -x <tempFolder>:          (Required) Temponary files folder on disk
EOF
  exit 1
}

findIntegrations="False"

while getopts "d:t:r:o:i:f:x:" o; do
  case "${o}" in
    (d) data=${OPTARG};;
    (t) filetype=${OPTARG};;
    (r) reference=$(realpath ${OPTARG});;
    (x) temp=$(realpath ${OPTARG});;
    (o) output=$(realpath ${OPTARG});;
    (f) findIntegrations=${OPTARG};;
    (i) input=$(realpath ${OPTARG});;
  esac
done
shift "$((OPTIND - 1))"

optionalParams=""
if [ ! -n "$findIntegrations" ]; then optionalParams="--integration find"; else optionalParams=""; fi


# Path to required files for docker volumes
input_folder="$(dirname "${input}")"
input_file="$(basename "${input}")"

docker run --rm \
    -v ${temp}:/usr/local/bin/binchicken/temp \
    -v ${reference}:/mnt/reference \
    -v ${output}:/mnt/results \
    -v ${input_folder}:/mnt/data \
    binchicken:0.1 bash -c "python3 run.py \
                          --data ${data} \
                          --filetype ${filetype} \
                          ${optionalParams} \
                          --sample /mnt/data/${input_file} \
                          --reference /mnt/reference/ \
                          --output /mnt/results/"
